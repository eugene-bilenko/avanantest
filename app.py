from flask import Flask
from flask import render_template
from flask import request
from stackoverflowapi import StackoverflowAPI
import settings

app = Flask(__name__)
soapi = StackoverflowAPI(settings.STACKAPPS_API_KEY)

@app.route('/')
def main():
    return render_template('main.html', stackapps_key=settings.STACKAPPS_API_KEY)

@app.route('/posts', methods=['GET','POST'])
def posts():
    token = request.args.get('token')
    if token:
        user = soapi.me(token)
    else:
        user = soapi.user(request.form['uid'])

    try:
        user = user.get('items')[0]
    except:
        return 'User id not found'

    posts = soapi.user_posts(user.get('user_id')).get('items', [])
    return render_template('posts.html', posts=posts, user=user)

if __name__ == '__main__':
    app.run()
