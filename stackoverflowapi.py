import gzip
import io
import json
import urllib

class StackoverflowAPI:
    base_url = 'https://api.stackexchange.com/'
    api_version = '2.2'

    def __init__(self, api_key=None):
        self.api_key = api_key

    def _request(self, path, params=None, access_token=None):
        if params is None:
            params = {}
        url = '{base_url}{api_version}/{path}'.format(base_url=self.base_url, api_version=self.api_version, path=path)

        params.update({
            'site': 'stackoverflow',
            'key': self.api_key,
        })

        done = False
        for k, v in params.items():
            if not done:
                url += '?'
                done = True
            else:
                url += '&'

            url += '%s=%s' % (k, urllib.parse.quote(str(v).encode('utf-8')))

        request = urllib.request.Request(url)

        request.add_header('Accept-encoding', 'gzip')
        req_open = urllib.request.build_opener()

        try:
            conn = req_open.open(request)
            info = conn.info()
            req_data = conn.read()
        except urllib.error.HTTPError as e:
            info = getattr(e, 'headers', {})
            req_data = e.read()


        data = self.get_data(info, req_data)

        return json.loads(data.decode('utf8'))

    def get_data(self, info, req_data):
        if info.get('Content-Encoding') == 'gzip':
            data_stream = io.BytesIO(req_data)
            gzip_stream = gzip.GzipFile(fileobj = data_stream)

            actual_data = gzip_stream.read()
        else:
            actual_data = req_data

        return actual_data

    def me(self, access_token):
        return self._request('me', {
            'access_token': access_token,
        })

    def user(self, uid):
        return self._request('users/{uid}'.format(uid=uid))

    def user_posts(self, uid):
        return self._request('users/{uid}/posts'.format(uid=uid))
