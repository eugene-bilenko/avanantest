## Quick guide

1. Install virtualenv https://virtualenv.pypa.io/en/stable/installation/

2. Create environment: virtualenv -p /usr/bin/python3 .env
https://virtualenv.pypa.io/en/stable/reference/#configuration

3. Activate environment source .env/bin/activate
https://virtualenv.pypa.io/en/stable/userguide/#activate-script

4. Install requirements pip install -r requirements.txt

5. Run server using gunicorn: gunicorn app:app -b 0.0.0.0:80

NOTE: in order to use StackExchange api https://api.stackexchange.com/docs/ locally, it is necessary to use 80 port, as local access to it is not always available - sometimes it may need to run the command of 5 point from the root.

6. Now the page is available by the url: http://localhost/